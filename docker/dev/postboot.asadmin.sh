#!/usr/bin/env bash
create-jdbc-connection-pool --datasourceclassname com.mysql.jdbc.jdbc2.optional.MysqlDataSource --restype javax.sql.DataSource --property user=root:password=password:DatabaseName=tailordb:ServerName=172.17.0.1:port=3306:url=jdbc\:mysql\://172.17.0.1\:3306/tailordb:useSSL=false measurementPU
create-jdbc-resource --connectionpoolid measurementPU jdbc/measureDb

create-jms-resource --restype javax.jms.TopicConnectionFactory --property addressList='172.17.0.2\:7676' --description "connection factory for measurement app" jms/KkCon
create-jms-resource --restype javax.jms.Topic jms/kkNewUserAccountCreatedTopic

deploy --name=msrmnt deployments/measurement-app-0.0.1.war
