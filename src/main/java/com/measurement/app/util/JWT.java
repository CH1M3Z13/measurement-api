package com.measurement.app.util;

import com.measurement.app.pojo.AppUser;
import com.measurement.app.util.exception.GeneralAppException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.crypto.spec.SecretKeySpec;
import javax.ejb.Stateless;
import javax.ws.rs.core.Response;
import javax.xml.bind.DatatypeConverter;
import java.security.Key;
import java.util.Date;

/**
 * @author buls
 */
@Stateless
public class JWT {
   SecretKey secretKey = new SecretKey();
   private final String issuer = "alcatraz";

   public String createJWT(AppUser appUser, long ttlMillis) {

       SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

       long nowMillis = System.currentTimeMillis();
       Date now = new Date(nowMillis);

       byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(secretKey.getSecret());
       Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

       JwtBuilder builder = Jwts.builder()
               .setIssuedAt(now)
               .setSubject(appUser.getEmail())
               .setIssuer(issuer)
               .signWith(signatureAlgorithm, signingKey);

       if (ttlMillis >= 0) {
           long expMillis = nowMillis + ttlMillis;
           Date exp = new Date(expMillis);
           builder.setExpiration(exp);
       }

       return builder.compact();
   }

   public Claims parseJWT(String jwt) {

       Claims claims = Jwts.parser()
               .setSigningKey(DatatypeConverter.parseBase64Binary(secretKey.getSecret()))
               .parseClaimsJws(jwt).getBody();

       return claims;
   }

    public Claims verifyJwt(String rawToken, String resource) throws GeneralAppException {
        try {
            String authToken = rawToken.substring(7);
            return parseJWT(authToken);
        }  catch (Exception e) {
            throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, "Invalid token supplied", "Token supplied could not be parsed",
                    resource);
        }
    }
}
