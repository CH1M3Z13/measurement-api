/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.measurement.app.manager;

import com.measurement.app.util.exception.GeneralAppException;

public interface ExceptionThrowerManagerLocal {
   
    void throwNullPreferenceAttributesException(String link) throws GeneralAppException;
    
    void throwInvalidTokenException (String link) throws GeneralAppException;
    
    void throwInvalidIntegerAttributeException (String link) throws GeneralAppException;
    
    void throwInvalidDoubleAttributeException (String link) throws GeneralAppException;
    
    void throwInvalidEmailAddressException (String link) throws GeneralAppException;

    void throwInvalidStatusTypeException(String link) throws GeneralAppException;

    void throwGeneralException (String link) throws GeneralAppException;

    void throwInvalidUserInTokenException (String link) throws GeneralAppException;

    void throwNullParametersException(String link) throws GeneralAppException;

    void throwUserAlreadyExistException(String link) throws GeneralAppException;

    void throwEmailAlreadyExistException(String link) throws GeneralAppException;

    void throwInvalidEmailFormatException(String link) throws GeneralAppException;

    void throwUserDoesNotExistException(String link) throws GeneralAppException;

    void throwInvalidLoginCredentialsException(String link) throws GeneralAppException;

    void throwInvalidLoginType(String link) throws GeneralAppException;

    void throwInvalidEmail(String link) throws GeneralAppException;

    void throwInvalidSocialPlatform(String link) throws GeneralAppException;

    void throwNoEmailLoginAllowedException(String link) throws GeneralAppException;

    void throwOrganisationDoesNotExistException(String link) throws GeneralAppException;

    void throwLocationPointDoesNotExistException(String link) throws GeneralAppException;

    void throwDestinationDoesNotExistException(String link) throws GeneralAppException;

    void throwLocationConfigNotExistException(String link) throws GeneralAppException;
}
