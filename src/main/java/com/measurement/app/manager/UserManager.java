package com.measurement.app.manager;

import com.measurement.app.data.manager.UserDataManagerLocal;
import com.measurement.app.model.User;
import com.measurement.app.pojo.AppUser;
import com.measurement.app.util.JWT;
import com.measurement.app.util.MD5;
import com.measurement.app.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * @author chimezie on 12/16/20.
 */
@Stateless
public class UserManager implements UserManagerLocal {

    private long SIXTY_MINUTES_MILLISECONDS = 3600000l;

    @EJB
    UserDataManagerLocal userDataManager;


    @Override
    public AppUser register(String firstName, String lastName,
                            String email, String phoneNumber, String password) throws GeneralAppException {
            User newCustomer = new User();
            newCustomer.setFirstName(firstName);
            newCustomer.setLastName(lastName);
            newCustomer.setEmail(email);
            newCustomer.setPhoneNumber(phoneNumber);
            newCustomer.setPassword(MD5.hash(password));

            newCustomer = userDataManager.createUser(newCustomer);
            System.out.println("Registration successful");
            AppUser appCustomer = getAppUser(newCustomer);
           // appCustomer = getAppUserWithToken(appCustomer);

        return appCustomer;
    }

    @Override
    public AppUser authenticateUser(String email, String password){
        User user = userDataManager.getUser(email);
        if (user != null && user.getPassword().equals(MD5.hash(password))) {
            System.out.println("USER LOGGED IN");
            AppUser appUser = getAppUser(user);
            appUser = getAppUserWithToken(appUser);

            return appUser;
        }else {
            return null;
        }

    }

    private AppUser getAppUser(User customer) {
        AppUser appCustomer = new AppUser();
        if (customer != null) {
            appCustomer.setFirstName(customer.getFirstName() == null ? "" : customer.getFirstName());
            appCustomer.setLastName(customer.getLastName() == null ? "" : customer.getLastName());
            appCustomer.setEmail(customer.getEmail() == null ? "" : customer.getEmail());
            appCustomer.setPhoneNumber(customer.getPhoneNumber() == null ? "" : customer.getPhoneNumber());
            //appCustomer.setPassword(customer.getPassword());
        }

        return appCustomer;
    }

    private AppUser getAppUserWithToken(AppUser appUser) {
        JWT jwt = new JWT();
        String token = jwt.createJWT(appUser, SIXTY_MINUTES_MILLISECONDS);
        appUser.setToken(token);

        return appUser;
    }
}
