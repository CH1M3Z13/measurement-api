package com.measurement.app.manager;

import com.measurement.app.pojo.AppUser;
import com.measurement.app.util.exception.GeneralAppException;

/**
 * @author chimezie on 12/16/20.
 */
public interface UserManagerLocal {
    AppUser register(String firstName, String lastName,
                     String email, String phoneNumber,
                     String password) throws GeneralAppException;

    AppUser authenticateUser(String email, String password);
}
