/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.measurement.app.manager;

import javax.ejb.Stateless;
import javax.ws.rs.core.Response;

import com.measurement.app.manager.ExceptionThrowerManagerLocal;
import com.measurement.app.util.exception.GeneralAppException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author buls
 */

@Stateless
public class ExceptionThrowerManager implements ExceptionThrowerManagerLocal {
    
    private final String INCOMPLETE_DATA_ERROR = "Incomplete data";
    private final String INCOMPLETE_DATA_ERROR_DETAILS = "Incomplete data has been provided";

    private final String INVALID_TOKEN_ERROR = "Invalid or expired token supplied";
    private final String INVALID_TOKEN_ERROR_DETAILS = "The auth token supplied is invalid or expired"; 

    private final String INVALID_INTEGER_VALUE_ERROR = "Integer value expected";
    private final String INVALID_INTEGER_VALUE_ERROR_DETAILS = "Could not parse value supplied. Integer value expected";
    
    private final String INVALID_DOUBLE_VALUE_ERROR = "Double value expected";
    private final String INVALID_DOUBLE_VALUE_ERROR_DETAILS = "Could not parse value supplied. Double value expected";
    
    private final String INVALID_EMAIL_ADDRESS_ERROR = "Invalid email address";
    private final String INVALID_EMAIL_ADDRESS_ERROR_DETAILS = "Invalid email address provided";

    private final String INVALID_STATUS_TYPE_ERROR= "Status type does not exist or is not allowed";
    private final String INVALID_STATUS_TYPE_ERROR_DETAILS = "The specified status type is invalid or is not allowed";

    private final String ERROR = "An exception occured.";
    private final String ERROR_DETAILS = "Error details: ";
    
    private final String INVALID_USER_IN_TOKEN_ERROR= "Could not find valid username details in token. Please re-initiate the process."; 
    private final String INVALID_USER_IN_TOKEN_ERROR_DETAILS = "The auth token does not contain a valid username";

    public final String INVALID_EMAIL_FORMAT_ERROR = "Invalid email";
    public final String INVALID_EMAIL_FORMAT_ERROR_DETAILS = "Invalid email format";

    public final String USERNAME_ALREADY_EXISTS_ERROR = "User already exists";
    public final String USERNAME_ALREADY_EXISTS_ERROR_DETAILS = "A user already exists with the username supplied";

    public final String EMAIL_ALREADY_EXISTS_ERROR = "Email already exists";
    public final String EMAIL_ALREADY_EXISTS_ERROR_DETAILS = "A user with that email already exists";

    private final String USER_NOT_FOUND_ERROR = "User not found";
    private final String USER_NOT_FOUND_ERROR_DETAILS = "The user with the supplied details id does not exist";

    private final String INVALID_LOGIN_CREDENTIALS_ERROR = "Invalid login credentials";
    private final String INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS = "Invalid Username/Password combination";

    private final String INVALID_LOGIN_TYPE_ERROR = "Invalid login type";
    private final String INVALID_LOGIN_TYPE_ERROR_DETAILS = "invalid login type supplied";

    private final String INVALID_EMAIL_ERROR = "Email not found";
    private final String INVALID_EMAIL_ERROR_DETAILS = "Email does not exist in database";

    private final String INVALID_SOCIAL_PLATFORM_ERROR = "Invalid social platform";
    private final String INVALID_SOCIAL_PLATFORM_ERROR_DETAILS = "Invalid social platform specified";

    private final String NO_EMAIL_LOGIN_ALLOWED_ERROR = "No email account found. Please try social login";
    private final String NO_EMAIL_LOGIN_ALLOWED_ERROR_DETAILS = "User created account via social media. Cannot login with email.";

    public final String ORGANISATION_DOES_NOT_EXIST_ERROR = "Organisation does not exists";
    public final String ORGANISATION_DOES_NOT_EXIST_ERROR_DETAILS = "An organisation does exists with the id supplied";

    public final String LOCATION_POINT_DOES_NOT_EXIST_ERROR = "Location point does not exists";
    public final String LOCATION_POINT_DOES_NOT_EXIST_ERROR_DETAILS = "A location point does exists with the id supplied";

    public final String DESTINATION_DOES_NOT_EXIST_ERROR = "Destination does not exists";
    public final String DESTINATION_DOES_NOT_EXIST_ERROR_DETAILS = "A destination does exists with the id supplied";

    public final String LOCATION_CONFIG_DOES_NOT_EXIST_ERROR = "Location config does not exists";
    public final String LOCATION_CONFIG_DOES_NOT_EXIST_ERROR_DETAILS = "A location config does exists with the id supplied";


    @Override
    public void throwNullPreferenceAttributesException(String link) throws GeneralAppException {
        log(link, INCOMPLETE_DATA_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                    400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }
        
    @Override
    public void throwInvalidTokenException(String link) throws GeneralAppException {
        log(link, INVALID_TOKEN_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_TOKEN_ERROR, INVALID_TOKEN_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidIntegerAttributeException (String link) throws GeneralAppException{
        log(link, INVALID_INTEGER_VALUE_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_INTEGER_VALUE_ERROR, INVALID_INTEGER_VALUE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidDoubleAttributeException (String link) throws GeneralAppException{
        log(link, INVALID_DOUBLE_VALUE_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_DOUBLE_VALUE_ERROR, INVALID_DOUBLE_VALUE_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidEmailAddressException (String link) throws GeneralAppException {
        log(link, INVALID_EMAIL_ADDRESS_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_ADDRESS_ERROR, INVALID_EMAIL_ADDRESS_ERROR_DETAILS,
                link);
    }
    
    @Override
    public void throwInvalidStatusTypeException(String link) throws GeneralAppException {
        log(link, INVALID_STATUS_TYPE_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_STATUS_TYPE_ERROR, INVALID_STATUS_TYPE_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwGeneralException (String link) throws GeneralAppException {
        log(link, ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, ERROR, ERROR_DETAILS, link);
    }
    
    @Override
    public void throwInvalidUserInTokenException (String link) throws GeneralAppException {
        log(link, INVALID_USER_IN_TOKEN_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_USER_IN_TOKEN_ERROR, INVALID_USER_IN_TOKEN_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwNullParametersException(String link) throws GeneralAppException {
        log(link, INCOMPLETE_DATA_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INCOMPLETE_DATA_ERROR, INCOMPLETE_DATA_ERROR_DETAILS, link);
    }

    @Override
    public void throwUserAlreadyExistException(String link) throws GeneralAppException {
        log(link, USERNAME_ALREADY_EXISTS_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USERNAME_ALREADY_EXISTS_ERROR, USERNAME_ALREADY_EXISTS_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwEmailAlreadyExistException (String link) throws GeneralAppException {
        log(link, EMAIL_ALREADY_EXISTS_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, EMAIL_ALREADY_EXISTS_ERROR, EMAIL_ALREADY_EXISTS_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidEmailFormatException(String link) throws GeneralAppException {
        log(link, INVALID_EMAIL_FORMAT_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_FORMAT_ERROR, INVALID_EMAIL_FORMAT_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwUserDoesNotExistException(String link) throws GeneralAppException {
        log(link, USER_NOT_FOUND_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, USER_NOT_FOUND_ERROR, USER_NOT_FOUND_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidLoginCredentialsException(String link) throws GeneralAppException {
        log(link, INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LOGIN_CREDENTIALS_ERROR, INVALID_LOGIN_CREDENTIALS_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidLoginType(String link) throws GeneralAppException {
        log(link, INVALID_LOGIN_TYPE_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_LOGIN_TYPE_ERROR, INVALID_LOGIN_TYPE_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidEmail(String link) throws GeneralAppException {
        log(link, INVALID_EMAIL_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_EMAIL_ERROR, INVALID_EMAIL_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwInvalidSocialPlatform(String link) throws GeneralAppException {
        log(link, INVALID_SOCIAL_PLATFORM_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, INVALID_SOCIAL_PLATFORM_ERROR, INVALID_SOCIAL_PLATFORM_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwNoEmailLoginAllowedException(String link) throws GeneralAppException {
        log(link, NO_EMAIL_LOGIN_ALLOWED_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, NO_EMAIL_LOGIN_ALLOWED_ERROR, NO_EMAIL_LOGIN_ALLOWED_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwOrganisationDoesNotExistException(String link) throws GeneralAppException {
        log(link, ORGANISATION_DOES_NOT_EXIST_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, ORGANISATION_DOES_NOT_EXIST_ERROR, ORGANISATION_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwLocationPointDoesNotExistException(String link) throws GeneralAppException {
        log(link, LOCATION_POINT_DOES_NOT_EXIST_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, LOCATION_POINT_DOES_NOT_EXIST_ERROR, LOCATION_POINT_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwDestinationDoesNotExistException(String link) throws GeneralAppException {
        log(link, DESTINATION_DOES_NOT_EXIST_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, DESTINATION_DOES_NOT_EXIST_ERROR, DESTINATION_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }

    @Override
    public void throwLocationConfigNotExistException(String link) throws GeneralAppException {
        log(link, LOCATION_CONFIG_DOES_NOT_EXIST_ERROR_DETAILS);
        throw new GeneralAppException(Response.Status.BAD_REQUEST.getStatusCode(),
                400, LOCATION_CONFIG_DOES_NOT_EXIST_ERROR, LOCATION_CONFIG_DOES_NOT_EXIST_ERROR_DETAILS,
                link);
    }

    private void log(String link, String errorDetails) {
        Logger.getLogger(ExceptionThrowerManager.class.getName()).log(Level.SEVERE, link + " | " + errorDetails);
    }

}

