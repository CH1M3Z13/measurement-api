package com.measurement.app.manager;

import com.measurement.app.data.manager.MeasurementDataManagerLocal;
import com.measurement.app.model.Measurement;
import com.measurement.app.pojo.AppMeasurement;

import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * @author chimezie on 12/11/20.
 */

@Stateless
public class MeasurementManager implements MeasurementManagerLocal {

    @EJB
    MeasurementDataManagerLocal measurementDataManager;

    @Override
    public AppMeasurement addClientMeasurement(String email, String length,
                                               String chestWidth, String armLength,
                                               String shoulderLength, String waistSize,
                                               String trouserLength, String thighSize){
        Measurement newMeasurement = new Measurement();
        newMeasurement.setEmail(email);
        newMeasurement.setLength(length);
        newMeasurement.setChestWidth(chestWidth);
        newMeasurement.setArmLength(armLength);
        newMeasurement.setShoulderLength(shoulderLength);
        newMeasurement.setWaistSize(waistSize);
        newMeasurement.setTrouserLength(trouserLength);
        newMeasurement.setThighSize(thighSize);

        measurementDataManager.createNewMeasurement(newMeasurement);
        AppMeasurement customerMeasurement = getAppMeasurement(newMeasurement);
        System.out.println(customerMeasurement);

        return customerMeasurement;
    }


    @Override
    public AppMeasurement getMeasurement(String email) {
       /*
        AppMeasurement measurementId = getMeasurementId(email);
        Measurement measurement = measurementDataManager.get(measurementId);

        */

        return  null;
    }

    private AppMeasurement getAppMeasurement(Measurement measurement) {
        AppMeasurement appMeasurement = new AppMeasurement();
        if(measurement != null){
            appMeasurement.setId(measurement.getId());
            appMeasurement.setLength(measurement.getLength());
            appMeasurement.setChestWidth(measurement.getChestWidth());
            appMeasurement.setArmLength(measurement.getArmLength());
            appMeasurement.setShoulderLength(measurement.getShoulderLength());
            appMeasurement.setWaistSize(measurement.getWaistSize());
            appMeasurement.setTrouserLength(measurement.getTrouserLength());
            appMeasurement.setThighSize(measurement.getThighSize());
        }

        return appMeasurement;
    }
}
