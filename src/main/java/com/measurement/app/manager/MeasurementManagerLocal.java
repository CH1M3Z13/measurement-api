package com.measurement.app.manager;

import com.measurement.app.pojo.AppMeasurement;

/**
 * @author chimezie on 12/11/20.
 */
public interface MeasurementManagerLocal {
    AppMeasurement addClientMeasurement(String email, String length, String chestWidth,
                                           String armLength, String shoulderLength,
                                           String waistSize, String trouserLength,
                                           String thighSize);

    AppMeasurement getMeasurement(String email);
}
