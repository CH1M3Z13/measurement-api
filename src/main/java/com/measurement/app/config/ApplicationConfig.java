package com.measurement.app.config;

import com.measurement.app.service.MeasurementService;
import com.measurement.app.service.UserService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author chimezie on 12/17/20.
 */

@javax.ws.rs.ApplicationPath("/api")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> s = new HashSet<Class<?>>();

        s.add(UserService.class);
        s.add(MeasurementService.class);

        return s;
    }
}
