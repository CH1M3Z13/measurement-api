package com.measurement.app.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author chimezie on 12/11/20.
 */
@Entity
@Table(name = "measurement")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Measurement.findAll", query = "SELECT p FROM Measurement p"),
        @NamedQuery(name = "Measurement.findById", query = "SELECT p FROM Measurement p WHERE p.id = :id")})
public class Measurement implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NotNull
    @Column(name = "measurement_id")
    private int id;

    @Column(name = "email")
    private String email;

    @Column(name = "length")
    private String length;

    @Column(name = "chest_width")
    private String chestWidth;

    @Column(name = "arm_length")
    private String armLength;

    @Column(name = "shoulder_length")
    private String shoulderLength;

    @Column(name = "waist_size")
    private String waistSize;

    @Column(name = "trouser_length")
    private String trouserLength;

    @Column(name = "thigh_size")
    private String thighSize;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getChestWidth() {
        return chestWidth;
    }

    public void setChestWidth(String chestWidth) {
        this.chestWidth = chestWidth;
    }

    public String getArmLength() {
        return armLength;
    }

    public void setArmLength(String armLength) {
        this.armLength = armLength;
    }

    public String getShoulderLength() {
        return shoulderLength;
    }

    public void setShoulderLength(String shoulderLength) {
        this.shoulderLength = shoulderLength;
    }

    public String getWaistSize() {
        return waistSize;
    }

    public void setWaistSize(String waistSize) {
        this.waistSize = waistSize;
    }

    public String getTrouserLength() {
        return trouserLength;
    }

    public void setTrouserLength(String trouserLength) {
        this.trouserLength = trouserLength;
    }

    public String getThighSize() {
        return thighSize;
    }

    public void setThighSize(String thighSize) {
        this.thighSize = thighSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Measurement that = (Measurement) o;
        return id == that.id && Objects.equals(email, that.email) && Objects.equals(length, that.length) && Objects.equals(chestWidth, that.chestWidth) && Objects.equals(armLength, that.armLength) && Objects.equals(shoulderLength, that.shoulderLength) && Objects.equals(waistSize, that.waistSize) && Objects.equals(trouserLength, that.trouserLength) && Objects.equals(thighSize, that.thighSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, length, chestWidth, armLength, shoulderLength, waistSize, trouserLength, thighSize);
    }
}
