package com.measurement.app.data.manager;

import com.measurement.app.data.provider.DataProviderLocal;
import com.measurement.app.model.User;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * @author chimezie on 12/16/20.
 */
@Stateless
public class UserDataManager implements UserDataManagerLocal {
    @EJB
    private DataProviderLocal crud;

    @Override
    public User createUser(User customer){
            return crud.create(customer);
    }

    @Override
    public List<User> getAllUsers() {
        return crud.findAll(User.class);
    }

    @Override
    public User getUser(String email) {
        return crud.find(email, User.class);
    }
}
