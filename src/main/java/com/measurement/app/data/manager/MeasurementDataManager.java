package com.measurement.app.data.manager;

import com.measurement.app.data.provider.DataProviderLocal;
import com.measurement.app.model.Measurement;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

/**
 * @author chimezie on 2/9/21.
 */

@Stateless
public class MeasurementDataManager implements MeasurementDataManagerLocal {
    @EJB
    DataProviderLocal crud;

    @Override
   public Measurement createNewMeasurement(Measurement measurement) {
        crud.create(measurement);

        return measurement;
    }

    @Override
    public List<Measurement> getAll() {
        return crud.findAll(Measurement.class);
    }

    @Override
    public Measurement getMeasurement(int measurementId) {

        return crud.find(measurementId, Measurement.class);
    }

    @Override
    public Measurement get(String email) {
        return crud.find(email, Measurement.class);
    }
}
