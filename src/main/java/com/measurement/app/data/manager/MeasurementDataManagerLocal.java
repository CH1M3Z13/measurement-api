package com.measurement.app.data.manager;

import com.measurement.app.model.Measurement;

import java.util.List;

/**
 * @author chimezie on 2/9/21.
 */
public interface MeasurementDataManagerLocal {
    Measurement createNewMeasurement(Measurement measurement);

    Measurement getMeasurement(int measurementId);

    Measurement get(String email);

    List<Measurement> getAll();
}
