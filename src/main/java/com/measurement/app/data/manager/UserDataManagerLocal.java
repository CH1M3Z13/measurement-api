package com.measurement.app.data.manager;

import com.measurement.app.model.User;
import javax.ejb.Local;
import java.util.List;

/**
 * @author chimezie on 12/16/20.
 */
@Local
public interface UserDataManagerLocal {

    User createUser(User customer);

    List<User> getAllUsers();

    User getUser(String email);

}
