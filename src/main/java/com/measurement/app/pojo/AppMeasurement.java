package com.measurement.app.pojo;

import java.util.Objects;

/**
 * @author chimezie on 12/11/20.
 */
public class AppMeasurement {
    private int id;
    private String email;
    private String length;
    private String chestWidth;
    private String armLength;
    private String shoulderLength;
    private String waistSize;
    private String trouserLength;
    private String thighSize;

    public AppMeasurement(){

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getChestWidth() {
        return chestWidth;
    }

    public void setChestWidth(String chestWidth) {
        this.chestWidth = chestWidth;
    }

    public String getArmLength() {
        return armLength;
    }

    public void setArmLength(String armLength) {
        this.armLength = armLength;
    }

    public String getShoulderLength() {
        return shoulderLength;
    }

    public void setShoulderLength(String shoulderLength) {
        this.shoulderLength = shoulderLength;
    }

    public String getWaistSize() {
        return waistSize;
    }

    public void setWaistSize(String waistSize) {
        this.waistSize = waistSize;
    }

    public String getTrouserLength() {
        return trouserLength;
    }

    public void setTrouserLength(String trouserLength) {
        this.trouserLength = trouserLength;
    }

    public String getThighSize() {
        return thighSize;
    }

    public void setThighSize(String thighSize) {
        this.thighSize = thighSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppMeasurement that = (AppMeasurement) o;
        return id == that.id && Objects.equals(email, that.email) && Objects.equals(length, that.length) && Objects.equals(chestWidth, that.chestWidth) && Objects.equals(armLength, that.armLength) && Objects.equals(shoulderLength, that.shoulderLength) && Objects.equals(waistSize, that.waistSize) && Objects.equals(trouserLength, that.trouserLength) && Objects.equals(thighSize, that.thighSize);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, length, chestWidth, armLength, shoulderLength, waistSize, trouserLength, thighSize);
    }
}
