package com.measurement.app.service;

import com.measurement.app.manager.MeasurementManagerLocal;
import com.measurement.app.manager.UserManagerLocal;
import com.measurement.app.pojo.AppMeasurement;
import com.measurement.app.pojo.AppUser;
import com.measurement.app.util.exception.GeneralAppException;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author chimezie on 12/17/20.
 */
@Path("/v1/user")
public class UserService {
    @EJB
    UserManagerLocal userManager;

    @EJB
    MeasurementManagerLocal measurementManager;


    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/sign-up")
    public Response createCustomer( @QueryParam("first-name") String firstName,
                                      @QueryParam("last-name") String lastName,
                                      @QueryParam("email") String email,
                                      @QueryParam("phone-number" ) String phoneNumber,
                                      @QueryParam("password") String password) throws GeneralAppException {

        AppUser customer = userManager.register(firstName, lastName, email, phoneNumber, password);
        return Response.ok(customer).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/add-client-measurement")
    public AppMeasurement addClientMeasurement(@QueryParam("email") String email,
                                               @QueryParam("full-name") String fullName,
                                               @QueryParam("length") String length,
                                               @QueryParam("armLength") String armLength,
                                               @QueryParam("waistSize") String waistSize,
                                               @QueryParam("thighSize") String thighSize,
                                               @QueryParam("chestWidth") String chestWidth,
                                               @QueryParam("shoulderLength") String shoulderLength){

        AppMeasurement appMeasurement = measurementManager.addClientMeasurement(email, fullName, length, armLength,
                waistSize, thighSize, chestWidth, shoulderLength);

        return appMeasurement;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/login")
    public Response authenticateUser(@QueryParam("email") String email,
                                @QueryParam("password") String password) {
        AppUser appCustomer = userManager.authenticateUser(email, password);

        return Response.ok(appCustomer).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get-client-measurement/{email}")
    public Response getClientMeasurement(@PathParam("email") String email) {

        return null;
    }

}
